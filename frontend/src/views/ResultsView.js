import React from 'react';
import {motion} from "framer-motion"
import ExtraWords from '../data/BackupData'
import * as RiTa from 'rita'

function ListItem(props) {
  // Correct! There is no need to specify the key here:
  return <li>{props.value}</li>;
}

function NumberList(props) {
  const labels = props.labels;
  const listItems = labels ? labels.map((label) =>
    // Correct! Key should be specified inside the array.
    <ListItem key={label} value={label} />

  ):null;
  return (
    <ul>
      {listItems}
    </ul>
  );
}

const numbers = [1, 2, 3, 4, 5];

class ResultsView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            image: null,
            labels: null, 
            bagofwords: null, 
            quotes: [], 
            quoteIndex: 0,
        };
    }
    componentDidMount() {
        this.setState({
            image: localStorage.image
                ? localStorage.image
                : null
        })
        this.setState({
            labels: localStorage.labels
                ? JSON.parse(localStorage.labels)
                : null
        })

        this.setState({
          quotes: localStorage.labels
                ? JSON.parse(localStorage.labels)["quote"]
                : null
        })
        console.log(JSON.parse(localStorage.labels)["quote"])
        this.drawImageToCanvas()
    }

  

    isVowel(word) {
      return /^[aeiou]$/.test(word[0].toLowerCase())?`an ${word}`:`a ${word}`;
  }

    
    

    drawImageToCanvas() {
        let targetWidth, targetHeight;
        if(window.innerWidth>768){
          targetWidth = window.innerWidth*0.5
          targetHeight = targetWidth*(0.75)
        } else {
          targetWidth = window.innerWidth*0.9
          targetHeight = window.innerHeight*0.5
        }
   

        var canvas = document.getElementById("photo-canvas");
 
        var ctx = canvas.getContext("2d");
        ctx.imageSmoothingQuality = "high"
        canvas.width = targetWidth * window.devicePixelRatio;
        canvas.height = targetHeight * window.devicePixelRatio;
        canvas.style.width = `${targetWidth}px`;
        canvas.style.height = `${targetHeight}px`;
        // ctx.canvas.height = window.innerHeight;
        function scaleToFit(img){
            // get the scale
            var scale = Math.min(canvas.width / img.width, canvas.height / img.height);
            // get the top left position of the image
            var x = (canvas.width / 2) - (img.width / 2) * scale;
            var y = (canvas.height / 2) - (img.height / 2) * scale;
            ctx.drawImage(img, x, y, img.width * scale, img.height * scale);
        }
        var image = new Image();
        image.src = localStorage.baseImage

        image.onload =  () => {
          scaleToFit(image)
            
        };


        if(localStorage.image) {
          var image2 = new Image();
          image2.src = localStorage.image
  
          image2.onload =  () => {
            scaleToFit(image2)
              
          };
  
        }
        // canvas.

        
    }

    goBack() {
      var getUrl = window.location;
      var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" 
      console.log(baseUrl);
      window.location = baseUrl

    }

    handleClick() {
      this.setState({quoteIndex: (this.state.quoteIndex+1)%this.state.quotes.length})
    }
    render() {
 
        return (
            <div className="results-container">
              <button onClick={this.goBack.bind(this)}>Back</button>
              
              <canvas id="photo-canvas"></canvas>

              
                <div className="labels" onClick={this.handleClick.bind(this)}>

                  {this.state.quotes[this.state.quoteIndex]}
                </div>

                <p>Tap to get more quotes</p>
            </div>
        )
    }
}

export default ResultsView;